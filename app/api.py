import logging

import settings
from wurker.util import BotArgs
from wurker.core import Wurkman

from flask import (
    Blueprint, flash, g, redirect, render_template, request, url_for, jsonify, current_app
)
from werkzeug.exceptions import abort

from app.auth import api_auth_required
from app.db import get_db

bp = Blueprint('api', __name__, url_prefix="/api")

logger = logging.getLogger('gunicorn.error')


@bp.get('/status')
@bp.get('/drone/<drone_id>/status')
@api_auth_required
def status(drone_id:str=None):
    """Obtain the status of this Wurker (Queen or Drone)."""

    if drone_id and not settings.env.is_queen:
        return None, 404

    from wurker.subcommands.status import run as runcmd

    args = BotArgs(
        app_root = settings.env.app_root,
        drone = [drone_id] if drone_id else None
    )

    try:
        status = runcmd(args)
    except Exception as ex:
        return jsonify({"error": f"{ex}"}), 400

    info = (
        status if not drone_id
        else {} if not status or drone_id not in status
        else status[drone_id]
    )

    if "db_conf" in info and info["db_conf"]:
        for obfuscate in ["password", "dsn", "url"]:
            if obfuscate in info["db_conf"] and info["db_conf"][obfuscate]:
                info["db_conf"][obfuscate] = obfuscate.upper()

    return jsonify(info)


@bp.get('/show')
@bp.get('/show/<option>')
@bp.get('/show/<option>/<key>')
@bp.get('/drone/<drone_id>/show')
@bp.get('/drone/<drone_id>/show/<option>')
@bp.get('/drone/<drone_id>/show/<option>/<key>')
@api_auth_required
def show(option:str=None, key:str=None, drone_id:str=None):
    """Obtain the 'show' data for this Wurker (Queen or Drone)."""

    if drone_id and not settings.env.is_queen:
        return None, 404

    from wurker.subcommands.show import run as showcmd

    options = ["supervised", "scheduled", "ondemand", "hive", "command", "cron", "bee",]
    keyed = ["command", "cron", "bee",]
    if option:
        if option not in options:
            return jsonify({"error": f"Invalid option: '{option}'"}), 400
        if key and option not in keyed:
            return jsonify({"error": f"Option '{option}' does not accept arguments"}), 400

    kwargs = {
        "app_root": settings.env.app_root,
        "subcommand": "show",
        "drone": [drone_id] if drone_id else None,
    }

    if option:
        if option not in keyed:
            kwargs[option] = True
        else:
            kwargs[option] = key if key else True

    args = BotArgs(**kwargs)
    showman = Wurkman(args)

    try:
        show = showcmd(showman)
    except Exception as ex:
        return jsonify({"error": f"{ex}"}), 400
    finally:
        showman.session.remove()

    items = (
        show if not drone_id
        else {} if not show or drone_id not in show
        else show[drone_id]
    )

    return jsonify(items)


@bp.post('/control/<action>')
@bp.post('/drone/<drone_id>/control/<action>')
@api_auth_required
def control(action:str, drone_id:str=None):
    """Request a 'control' subcommand action for this Wurker (Queen or Drone)."""

    if drone_id and not settings.env.is_queen:
        return None, 404

    from wurker.subcommands.control import run as runcmd

    if action not in ["reload", "pause"]:
        return

    args = BotArgs(
        app_root = settings.env.app_root,
        subcommand = "control",
        action = action,
        drone = [drone_id] if drone_id else None
    )
    controlman = Wurkman(args)

    try:
        runcmd(controlman)
    except Exception as ex:
        return jsonify({"error": f"{ex}"}), 400
    finally:
        controlman.session.remove()

    return jsonify(True)


@bp.post('/add/<option>')
@bp.post('/drone/<drone_id>/add/<option>')
@api_auth_required
def add(option:str, drone_id:str=None):
    """Request an 'add' subcommand with required option and JSON payload (Queen or Drone)."""

    if drone_id and not settings.env.is_queen:
        return None, 404

    from wurker.subcommands.add import run as runcmd

    json_arg = request.json

    cron_slug = None
    if option == "bee" and "cron_slug" in json_arg:
        cron_slug = json_arg["cron_slug"]
        del json_arg["cron_slug"]

    args = BotArgs(
        app_root = settings.env.app_root,
        subcommand = "add",
        option = option,
        json = json_arg,
        drone = [drone_id] if drone_id else None
    )
    addman = Wurkman(args)

    try:
        runcmd(addman)
    except Exception as ex:
        return jsonify({"error": f"{ex}"}), 400
    finally:
        if not cron_slug:
            addman.session.remove()

    # Crons are mapped after adding a Bee, just like CLI. We allow users to combine the step in UI.
    if cron_slug:
        bee_slug = json_arg["slug"]

        args = BotArgs(
            app_root = settings.env.app_root,
            subcommand = "add",
            option = "map",
            json = {"wurker_bee_slug": bee_slug, "wurker_cron_slug": cron_slug},
            drone = [drone_id] if drone_id else None
        )
        addman = Wurkman(args)

        try:
            runcmd(addman)
        except Exception as ex:
            return jsonify({"error": f"{ex}"}), 400
        finally:
            addman.session.remove()

    return jsonify(True)


@bp.post('/edit/<option>/<key0>')
@bp.post('/edit/<option>/<key0>/<key1>')
@bp.post('/drone/<drone_id>/edit/<option>/<key0>')
@bp.post('/drone/<drone_id>/edit/<option>/<key0>/<key1>')
@api_auth_required
def edit(option:str, key0:str, key1:str=None, drone_id:str=None):
    """Request an 'edit' subcommand with required option and at least one key and JSON (Queen or Drone)."""

    if drone_id and not settings.env.is_queen:
        return None, 404

    from wurker.subcommands.edit import run as runcmd

    args = BotArgs(
        app_root = settings.env.app_root,
        subcommand = "edit",
        option = option,
        key = [key0] if not key1 else [key0, key1],
        json = request.json,
        drone = [drone_id] if drone_id else None
    )
    editman = Wurkman(args)

    try:
        runcmd(editman)
    except Exception as ex:
        return jsonify({"error": f"{ex}"}), 400
    finally:
        editman.session.remove()

    return jsonify(True)


@bp.post('/remove/<option>/<key0>')
@bp.post('/remove/<option>/<key0>/<key1>')
@bp.post('/drone/<drone_id>/remove/<option>/<key0>')
@bp.post('/drone/<drone_id>/remove/<option>/<key0>/<key1>')
def remove(option:str, key0:str, key1:str=None, drone_id:str=None):
    """Request a 'remove' subcommand with required option and at least one key (Queen or Drone)."""

    if drone_id and not settings.env.is_queen:
        return None, 404

    from wurker.subcommands.remove import run as runcmd

    args = BotArgs(
        app_root = settings.env.app_root,
        subcommand = "rm",
        option = option,
        key = [key0] if not key1 else [key0, key1],
        drone = [drone_id] if drone_id else None
    )
    rmman = Wurkman(args)

    try:
        runcmd(rmman)
    except Exception as ex:
        return jsonify({"error": f"{ex}"}), 400
    finally:
        rmman.session.remove()

    return jsonify(True)


@bp.post('/kill')
@bp.post('/kill/<option>')
@bp.post('/kill/<option>/<key>')
@bp.post('/drone/<drone_id>/kill')
@bp.post('/drone/<drone_id>/kill/<option>')
@bp.post('/drone/<drone_id>/kill/<option>/<key>')
@api_auth_required
def kill(option:str=None, key:str=None, drone_id:str=None):
    """Request a 'kill' subcommand with optional option and key (Queen or Drone)."""

    if drone_id and not settings.env.is_queen:
        return None, 404

    from wurker.subcommands.kill import run as runcmd

    kwargs = {
        "app_root": settings.env.app_root,
        "subcommand": "kill",
    }

    if not option:
        option = "hive"
    if not key or option in ["hive", "supervised", "scheduled", "ondemand"]:
        key = True

    kwargs[option] = key
    args = BotArgs(**kwargs)
    killman = Wurkman(args)

    try:
        runcmd(killman)
    except Exception as ex:
        return jsonify({"error": f"{ex}"}), 400
    finally:
        killman.session.remove()

    return jsonify(True)


@bp.get('/report/<slug>')
@bp.get('/report/<slug>/daterange/<start>/<end>')
@bp.get('/drone/<drone_id>/report/<slug>')
@bp.get('/drone/<drone_id>/report/<slug>/daterange/<start>/<end>')
@api_auth_required
def report(slug:str, drone_id:str='', start:str=None, end:str=None):
    """Obtain the 'report' data for this Wurker (Queen or Drone) and given Bee slug."""

    from wurker.subcommands.report import run as runcmd

    # Pagination and sorting
    page = int(request.args.get('page', 0))
    page_limit = int(request.args.get('limit', 20))
    sort = request.args.get('sort', 'id')
    sort_order = request.args.get('order', 'asc')

    args = BotArgs(
        app_root = settings.env.app_root,
        drone = [drone_id] if drone_id else None,
        option = "bee",
        key = slug,
        start = start,
        end = end,
        page = page,
        page_limit = page_limit,
        sort = sort,
        sort_order = sort_order,
    )
    wurkman = Wurkman(args)

    try:
        data = runcmd(wurkman)
    except Exception as ex:
        return jsonify({"error": f"{ex}"}), 400
    finally:
        wurkman.session.remove()

    report = (
        data if not drone_id
        else [] if not data or drone_id not in data
        else data[drone_id]
    )

    return jsonify(report)
