import logging
from datetime import datetime

import settings
from wurker.util import BotArgs
from wurker.core import Wurkman

from flask import (
    Blueprint, flash, g, redirect, render_template, request, url_for, send_from_directory, jsonify, Response,
    current_app
)
from werkzeug.exceptions import abort

from app.auth import login_required
from app.db import get_db


bp = Blueprint('report', __name__)

logger = logging.getLogger('gunicorn.error')


@bp.route('/report/<slug>')
@bp.route('/report/<slug>/daterange/<start>/<end>')
@bp.route('/drone/<drone_id>/report/<slug>')
@bp.route('/drone/<drone_id>/report/<slug>/daterange/<start>/<end>')
def report(slug:str, drone_id:str='', start:str=None, end:str=None):
    """View the report view this Wurker (Queen or Drone) and given Bee slug."""

    if drone_id and not settings.env.is_queen:
        return None, 404

    from wurker.subcommands.status import run as statuscmd

    # Will need the Queen's status for Node selectbox, even if this is a Drone
    queen_args = BotArgs(
        app_root = settings.env.app_root,
        subcommand = "status"
    )
    queen = statuscmd(queen_args)

    # Get this Wurker's status (Queen or Drone)
    status_args = BotArgs(
        app_root = settings.env.app_root,
        subcommand = "status",
        drone = [drone_id] if drone_id else None
    )

    status = statuscmd(status_args)
    info = (
        status if not drone_id
        else {} if not status or drone_id not in status
        else status[drone_id]
    )

    # Set daterange
    daterange = {}
    if start and end:
        daterange = {"start": start, "end": end}

    # Compose the result and render it
    result = {
        "page": info.get("name", None),
        "app_id": info.get("app_id", None),
        "is_queen": info.get("is_queen", None),
        "drones": queen.get("drones", None),
        "env": info.get("env", None),
        "report_slug": slug,
        "daterange": daterange,
        "thisYear": datetime.now().strftime('%Y'),   # For copyright
    }

    return render_template('report.html', **result)
