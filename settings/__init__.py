import os
import json

from slugify import slugify
from dotenv import load_dotenv
from dataclasses import dataclass, asdict, field


@dataclass
class DBConf:
    """A data class for DB config dependency-injection."""

    # Required fields
    database: str
    dbdriver: str

    # Optional driver-dependent fields
    hostname: str = ''
    username: str = ''
    password: str = field(default='', repr=False)
    port: int = 0

    # URL or DSN?
    url: str = ''
    dsn: str = ''


class EnvConf:
    """A class to encapsulate .env settings for dep-injection."""

    __slots__ = [
        "__dict__", "app_root", "name", "slug", "app_id", "env", "debug", "log", "loglevel", "is_queen",
        "db_hostname", "db_username", "db_password", "db_database", "db_driver", "db_port",
        "local_data_dir", "local_db_database", "local_db_driver", "app_db_database",
        "seed_dir", "seed_schema_sql", "seed_data_glob", "server_domain", "api_auth",
    ]

    url_format = "{dbdriver}:///{database}".format
    dsn_format = "{dbdriver}://{username}:{password}@{hostname}:{port}/{database}".format

    def __init__(self):
        # Init anything we can from .env prefixed by 'WURKER_'
        load_dotenv()
        for slot in [s for s in self.__slots__ if s not in ["__dict__", "app_root"]]:
            setattr(self, slot, os.getenv(f"WURKER_{slot.upper()}", None))

        # Boolean-ize the boolean-ish
        self.debug = (isinstance(self.debug, str) and self.debug.lower() in ['1', 'true'])
        self.is_queen = (isinstance(self.is_queen, str) and self.is_queen.lower() in ['1', 'true'])

        # Make a slug, if not set in .env
        if self.name and not self.slug:
            self.slug = slugify(self.name)

        self._init_paths()
        self._init_api_auth()

    def _init_paths(self):
        """Assemble some paths based on app_root and .env, with defaults."""

        self.app_root = os.path.dirname(os.path.dirname(os.path.abspath(__file__)))

        if not self.seed_dir:
            self.seed_dir = os.path.join("etc", "seeds")
        self.seed_dir = os.path.join(self.app_root, self.seed_dir)

        if not self.seed_data_glob:
            self.seed_data_glob = "seed_data_[0-9][0-9][0-9].json"

        if not self.local_data_dir:
            self.local_data_dir = os.path.join(self.app_root, "data")

        if not self.local_db_driver:
            self.local_db_driver = "sqlite"

        if not self.local_db_database:
            self.local_db_database = "hive.db"
        self.local_db_database = os.path.join(self.local_data_dir, self.local_db_database)

        if not self.app_db_database:
            self.app_db_database = "app.db"
        self.app_db_database = os.path.join(self.local_data_dir, self.app_db_database)

    def _init_api_auth(self):
        """We need local .gitignored plaintext drone API auth, if applicable."""

        self.api_auth = None

        api_auth_json = os.path.join(self.local_data_dir, "api_auth.json")
        if not os.path.exists(api_auth_json):
            return

        with open(api_auth_json, 'r') as f:
            users = json.load(f)
        if not users:
            return

        user = users.pop()
        if "username" not in user or "password" not in user:
            return

        self.api_auth = (user["username"], user["password"])

    @property
    def RemoteDB(self) -> DBConf:
        kwargs = {
            "hostname": self.db_hostname,
            "username": self.db_username,
            "password": self.db_password,
            "database": self.db_database,
            "dbdriver": self.db_driver,
            "port": int(self.db_port) if self.db_port else None,
        }
        kwargs["dsn"] = self.dsn_format(**kwargs)

        return DBConf(**kwargs)

    @property
    def LocalDB(self) -> DBConf:
        url = (
            self.url_format(dbdriver=self.local_db_driver, database=self.local_db_database)
            if self.local_db_driver == "sqlite"
            else ''
        )
        return DBConf(dbdriver=self.local_db_driver, database=self.local_db_database, url=url)

    @property
    def AppDB(self) -> DBConf:
        url = self.url_format(dbdriver="sqlite", database=self.app_db_database)
        return DBConf(dbdriver="sqlite", database=self.app_db_database, url=url)

    @property
    def ApiAuth(self) -> tuple:
        return self.api_auth

    def asdict(self, attr:str=''):
        if not attr:
            return {k:getattr(self, k) for k in self.__slots__}

        db_conf = None
        if attr == "RemoteDB":
            db_conf = self.RemoteDB
        elif attr == "LocalDB":
            db_conf = self.LocalDB

        if not db_conf:
            return {}

        return asdict(db_conf)


# Look at us all bad ass!
env = EnvConf()
