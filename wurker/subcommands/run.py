def run(wurkman):
    """Run Wurker Bees."""

    if (wurkman.args.ondemand
        or wurkman.args.supervised
        or wurkman.args.scheduled
        or wurkman.args.cron
        or wurkman.args.bee):

        return wurkman.run_once()

    return wurkman.run_forever()
