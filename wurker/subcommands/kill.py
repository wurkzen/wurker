import settings
from ..db import get_drone_status
from ..drone import make_drone_clients


def run_drone(args):
    """Run via Drone API(s)."""

    db_conf = settings.env.RemoteDB
    app_id = settings.env.app_id
    drone_status = get_drone_status(db_conf, app_id, drones=args.drone)
    if drone_status is None or not len(drone_status):
        print(f"WARNING: Queen ({app_id}) Drones have no 'wurker_status' data", file=sys.stderr)
        return

    drone_clients = make_drone_clients(drone_status, auth=settings.env.ApiAuth)

    for app_id, drone in drone_clients.items():
        drone.post_kill(args.option, args.key)


def run(wurkman):
    """Kill Wurker Bees, in various ways."""

    # Run via Drone API?
    if hasattr(wurkman.args, "drone") and wurkman.args.drone is not None:
        return run_drone(wurkman.args)

    # Kill all?
    filters = [
        wurkman.args.supervised, wurkman.args.scheduled, wurkman.args.cron, wurkman.args.bee, wurkman.args.ondemand
    ]
    if all(x is None for x in filters):
        return wurkman.kill()

    # Filter out!
    bees, bee_ct = wurkman.get_bees()

    if wurkman.args.supervised:
        bees = bees["supervised"]
    elif wurkman.args.scheduled:
        bees = bees["scheduled"]
    elif wurkman.args.ondemand:
        bees = bees["ondemand"]
    elif wurkman.args.hive:
        bees = wurkman.get_hive()

    # OK got some bees to kill (poor bees!)
    wurkman.kill(bees)
