import os
import pytest


def run(wurkman):
    testpath = os.path.join(wurkman.args.app_root, "tests")

    options = ["-x"]
    if wurkman.args.verbose:
        options.append("-v")
    options.append(testpath)

    return pytest.main(options)
