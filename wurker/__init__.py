import os
import sys
import glob
import json
import argparse

import settings
from .util import loglevels
from .db import make_engine
from wurker.subcommands import add_subparsers

from sqlalchemy.orm import sessionmaker, scoped_session


# Module-level engine and session objects
try:
    engine = make_engine(db_conf=settings.env.RemoteDB)
    session_factory = sessionmaker(bind=engine)
    Session = scoped_session(session_factory)
except Exception:
    engine = None
    Session = None


def parse_args(app_root):
    """Parse CLI args."""

    def_log = settings.env.log or "syslog"
    def_loglevel = settings.env.loglevel or "error"
    loglevels_l = list(loglevels.keys())

    # Default Stingers path needed before subparsers
    stingers_root = os.path.join(app_root, "etc", "stingers")

    # Setup and confirm seed paths needed before subparsers
    seed_data_glob = None
    seed_dir = settings.env.seed_dir
    if seed_dir:
        seed_dir = os.path.join(app_root, seed_dir)
        seed_data_glob = os.path.join(seed_dir, settings.env.seed_data_glob)

    # OK, make the parser and get on with it
    parser = argparse.ArgumentParser(
        prog="wurk", description="Run and manage arbitrary or python jobs"
    )
    parser.add_argument(
        "--log", default=def_log,
        help=f"Log to 'syslog', 'stdout', or '/path/to/file' (DEFAULT: '{def_log}')"
    )
    parser.add_argument(
        "--loglevel", default=def_loglevel, choices=loglevels_l,
        help=f"Log-level: {loglevels_l} (DEFAULT: '{def_loglevel}')"
    )

    # Add the command subparsers & parse
    add_subparsers(parser, app_root=app_root, stingers_root=stingers_root, seed_dir=seed_dir)
    args = parser.parse_args()
    args.app_root = app_root

    # If infile it will be JSON
    if hasattr(args, "json") and args.json:
        try:
            args.json = json.load(args.json)
        except Exception as ex:
            print(f"ERROR: Parsing JSON file {args.json.name}: {ex}", file=sys.stderr)
            raise ex

    # Seed is special. Did I mention that?
    if seed_dir and args.subcommand == "seed":
        args.data_paths = sorted(glob.glob(seed_data_glob))

    # Further arg parsing for filtered subcommands...
    if args.subcommand in ["show", "run", "kill"]:
        key_options = ["cron", "bee"]

        if args.subcommand == "show":
            args.log = "syslog"   # quiet mode
            key_options = ["command", "cron", "bee"]

        for option in args.options:
            setattr(args, option, False)

        if args.option:
            setattr(args, args.option, True)
            if args.option in key_options and args.key:
                setattr(args, args.option, args.key)

    return args
