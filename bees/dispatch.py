import sys
import logging

import settings
import bees.dispatchers
from wurker.util import log


def run(slug:str="", **kwargs):
    """
    Wurker Queen Drone Dispatch
    ---------------------------
    Implement the input reader callback function(s) in the 'bees.dispatchers' submodule.
    Required dispatcher module names must look like: 'dispatchers/cb_{slug}.py'
    Dispatcher modules must contain a function with signature like: 'callback(**kwargs)'
    Dispatcher callbacks must return an integer exit code.

    All errors are logged to the Queen's log. Exceptions are errors, but may also result in a NOK exit code.

    :param slug: A generic slug. Probably wurker_bee.slug. Used to find the dispatcher callback.
    :param **kwargs: Arbitrary keyword args passed to the callback

    :return: Integer exit code
    """

    if not settings.env.is_queen:
        log("This Wurker is not a Queen", level=logging.ERROR)
        return 1

    module_slug = f"cb_{slug}"
    if module_slug not in bees.dispatchers.callbacks:
        log(f"Wurker Drone Dispatcher callback does not exist: '{module_slug}'")
        return 2

    return bees.dispatchers.callbacks[module_slug](**kwargs)
