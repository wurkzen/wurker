import os
import pytest

import settings
from wurker.hive import Hive
from wurker.util import BotArgs
from wurker.core import Wurkman
from wurker.base import Bee, PyBee


@pytest.fixture
def app_root():
    return os.path.dirname(os.path.dirname(os.path.abspath(__file__)))

def test_dotenv_exists(app_root):
    dotenv_path = os.path.join(app_root, ".env")
    assert os.path.exists(dotenv_path)

def test_settings(app_root):
    assert settings.env.app_root == app_root
    assert settings.env.name == os.getenv("WURKER_NAME")
    assert settings.env.app_id == os.getenv("WURKER_APP_ID")
    assert settings.env.env == os.getenv("WURKER_ENV")
    assert settings.env.debug == (os.getenv("WURKER_DEBUG").lower() in ['true', '1'])
    assert settings.env.log == os.getenv("WURKER_LOG")
    assert settings.env.loglevel == os.getenv("WURKER_LOGLEVEL")
    assert settings.env.is_queen == (os.getenv("WURKER_IS_QUEEN").lower() in ['true', '1'])

    assert settings.env.db_hostname == os.getenv("WURKER_DB_HOSTNAME")
    assert settings.env.db_username == os.getenv("WURKER_DB_USERNAME")
    assert settings.env.db_password == os.getenv("WURKER_DB_PASSWORD")
    assert settings.env.db_database == os.getenv("WURKER_DB_DATABASE")
    assert settings.env.db_driver == os.getenv("WURKER_DB_DRIVER")
    assert settings.env.db_port == os.getenv("WURKER_DB_PORT")

    assert settings.env.server_domain == os.getenv("WURKER_SERVER_DOMAIN")

def test_bot_args(app_root):
    args = BotArgs(app_root=app_root)
    assert args.app_root == app_root
    assert args.return_result == True
    assert args.log == "syslog"
    assert args.loglevel == "debug"
    assert args.subcommand is None
    assert args.command is None
    assert args.bee is None
    assert args.cron is None
    assert args.supervised == False
    assert args.scheduled == False
    assert args.ondemand == False
    assert args.hive == False
    assert args.any == True
    assert args.option is None
    assert args.json is None

def test_init_wurkman(app_root):
    args = BotArgs(app_root=app_root)
    wurkman = Wurkman(args)

    assert wurkman.args == args
    assert wurkman.app_id == settings.env.app_id
    assert wurkman.loglevel == settings.env.loglevel

    assert wurkman.db_conf == settings.env.RemoteDB
    assert wurkman.session is not None
    assert isinstance(wurkman.hive, Hive)

    assert wurkman.slug_map is not None
    assert "bee" in wurkman.slug_map
    assert wurkman.slug_map["bee"] is not None
    assert "cron" in wurkman.slug_map
    assert wurkman.slug_map["cron"] is not None

def test_get_bees(app_root):
    args = BotArgs(app_root=app_root)
    args.subcommand = "run"
    wurkman = Wurkman(args)

    bees, _ = wurkman.get_bees()
    for wurk_type, wurk_data in bees.items():
        for cron_slug, cron_data in wurk_data.items():
            for bee_slug, bee_data in cron_data.items():
                assert isinstance(bee_data, Bee) or isinstance(bee_data, PyBee)

def test_get_hive(app_root):
    args = BotArgs(app_root=app_root)
    wurkman = Wurkman(args)

    hive = wurkman.get_hive()
    assert len(hive) == 0

def test_get_control(app_root):
    args = BotArgs(app_root=app_root)
    wurkman = Wurkman(args)

    control = wurkman.get_control()
    assert control is not None

def test_set_control(app_root):
    args = BotArgs(app_root=app_root)
    wurkman = Wurkman(args)

    control = wurkman.get_control()
    assert control.reload == False

    wurkman.set_control(reload=True)
    control = wurkman.get_control()
    assert control.reload == True

    wurkman.set_control(reload=False)
