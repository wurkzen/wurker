import os
import pytest

from wurker.core import Wurkman
from wurker.util import BotArgs
from wurker.subcommands.remove import run
from models.wurker import WurkerCommand, WurkerCron, WurkerBee

@pytest.fixture
def app_root():
    return os.path.dirname(os.path.dirname(os.path.abspath(__file__)))

def test_map(app_root):
    args = BotArgs(
        app_root = app_root,
        subcommand = "remove",
        option = "map",
        key = ["dummy_bee", "minutes_1"],
    )
    wurkman = Wurkman(args)
    run(wurkman)

    with wurkman.session() as db:
        bee = db.query(WurkerBee).filter(
            WurkerBee.slug == "dummy_bee",
            WurkerBee.app_id == wurkman.app_id
        ).first()

        assert bee is not None
        assert len(bee.crons) == 0

def test_bee(app_root):
    args = BotArgs(
        app_root = app_root,
        subcommand = "remove",
        option = "bee",
        key = ["dummy_bee"],
    )
    wurkman = Wurkman(args)
    run(wurkman)

    with wurkman.session() as db:
        bee = db.query(WurkerBee).filter(
            WurkerBee.slug == "dummy_bee",
            WurkerBee.app_id == wurkman.app_id
        ).first()

        assert bee is None

def test_cron(app_root):
    args = BotArgs(
        app_root = app_root,
        subcommand = "remove",
        option = "cron",
        key = ["dummy_cron"],
    )
    wurkman = Wurkman(args)
    run(wurkman)

    with wurkman.session() as db:
        cron = db.query(WurkerCron).filter(
            WurkerCron.slug == "dummy_cron",
            WurkerCron.app_id == wurkman.app_id
        ).first()

        assert cron is None

def test_command(app_root):
    args = BotArgs(
        app_root = app_root,
        subcommand = "remove",
        option = "command",
        key = ["dummy_command"],
    )
    wurkman = Wurkman(args)
    run(wurkman)

    with wurkman.session() as db:
        command = db.query(WurkerCommand).filter(
            WurkerCommand.slug == "dummy_command",
            WurkerCommand.app_id == wurkman.app_id
        ).first()

        assert command is None
