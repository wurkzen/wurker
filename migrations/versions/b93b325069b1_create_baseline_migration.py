"""Create baseline migration

Revision ID: b93b325069b1
Revises:
Create Date: 2024-09-10 23:49:25.890085

"""
from typing import Sequence, Union

from alembic import op
import sqlalchemy as sa
import sqlalchemy_utils


# revision identifiers, used by Alembic.
revision: str = 'b93b325069b1'
down_revision: Union[str, None] = None
branch_labels: Union[str, Sequence[str], None] = None
depends_on: Union[str, Sequence[str], None] = None


def upgrade() -> None:
    if not sa.inspect(op.get_bind()).has_table('wurker_control'):
        op.create_table('wurker_control',
                        sa.Column('app_id', sqlalchemy_utils.types.uuid.UUIDType(), nullable=False),
                        sa.Column('seed', sa.Integer(), nullable=True),
                        sa.Column('reload', sa.Boolean(), nullable=True),
                        sa.Column('pause', sa.Boolean(), nullable=True),
                        sa.PrimaryKeyConstraint('app_id')
                        )

    if not sa.inspect(op.get_bind()).has_table('wurker_command'):
        op.create_table('wurker_command',
                        sa.Column('id', sa.Integer(), nullable=False),
                        sa.Column('app_id', sqlalchemy_utils.types.uuid.UUIDType(), nullable=False),
                        sa.Column('slug', sa.String(length=36), nullable=False),
                        sa.Column('name', sa.String(length=36), nullable=False),
                        sa.Column('description', sa.Text(), nullable=True),
                        sa.Column('command', sa.String(length=128), nullable=True),
                        sa.Column('is_module', sa.Boolean(), nullable=False),
                        sa.ForeignKeyConstraint(['app_id'], ['wurker_control.app_id'], ),
                        sa.PrimaryKeyConstraint('id'),
                        sa.UniqueConstraint('app_id', 'slug')
                        )

    if not sa.inspect(op.get_bind()).has_table('wurker_cron'):
        op.create_table('wurker_cron',
                        sa.Column('id', sa.Integer(), nullable=False),
                        sa.Column('app_id', sqlalchemy_utils.types.uuid.UUIDType(), nullable=False),
                        sa.Column('slug', sa.String(length=64), nullable=False),
                        sa.Column('name', sa.String(length=64), nullable=False),
                        sa.Column('cron', sa.String(length=32), nullable=True),
                        sa.Column('overrun', sa.Integer(), nullable=False),
                        sa.ForeignKeyConstraint(['app_id'], ['wurker_control.app_id'], ),
                        sa.PrimaryKeyConstraint('id'),
                        sa.UniqueConstraint('app_id', 'slug')
                        )

    if not sa.inspect(op.get_bind()).has_table('wurker_status'):
        op.create_table('wurker_status',
                        sa.Column('app_id', sqlalchemy_utils.types.uuid.UUIDType(), nullable=False),
                        sa.Column('slug', sa.String(length=36), nullable=False),
                        sa.Column('name', sa.String(length=36), nullable=False),
                        sa.Column('is_queen', sa.Boolean(), nullable=True),
                        sa.Column('env', sa.String(length=36), nullable=False),
                        sa.Column('private_ip', sa.String(length=36), nullable=True),
                        sa.Column('public_ip', sa.String(length=36), nullable=True),
                        sa.Column('server_domain', sa.String(length=128), nullable=True),
                        sa.ForeignKeyConstraint(['app_id'], ['wurker_control.app_id'], ),
                        sa.PrimaryKeyConstraint('app_id')
                        )

    if not sa.inspect(op.get_bind()).has_table('wurker_bee'):
        op.create_table('wurker_bee',
                        sa.Column('id', sa.Integer(), nullable=False),
                        sa.Column('app_id', sqlalchemy_utils.types.uuid.UUIDType(), nullable=False),
                        sa.Column('slug', sa.String(length=128), nullable=False),
                        sa.Column('name', sa.String(length=128), nullable=False),
                        sa.Column('description', sa.Text(), nullable=True),
                        sa.Column('command_id', sa.Integer(), nullable=False),
                        sa.Column('args', sa.JSON(), nullable=True),
                        sa.Column('quiet_restart', sa.Boolean(), nullable=True),
                        sa.Column('enabled', sa.Boolean(), nullable=False),
                        sa.ForeignKeyConstraint(['app_id'], ['wurker_control.app_id'], ),
                        sa.ForeignKeyConstraint(['command_id'], ['wurker_command.id'], ),
                        sa.PrimaryKeyConstraint('id'),
                        sa.UniqueConstraint('app_id', 'slug')
                        )

    if not sa.inspect(op.get_bind()).has_table('wurker_bee_cron'):
        op.create_table('wurker_bee_cron',
                        sa.Column('wurker_bee_id', sa.Integer(), nullable=False),
                        sa.Column('wurker_cron_id', sa.Integer(), nullable=False),
                        sa.ForeignKeyConstraint(['wurker_bee_id'], ['wurker_bee.id'], ),
                        sa.ForeignKeyConstraint(['wurker_cron_id'], ['wurker_cron.id'], ),
                        sa.PrimaryKeyConstraint('wurker_bee_id', 'wurker_cron_id')
                        )

    if not sa.inspect(op.get_bind()).has_table('wurker_bee_report'):
        op.create_table('wurker_bee_report',
                        sa.Column('id', sa.Integer(), nullable=False),
                        sa.Column('app_id', sqlalchemy_utils.types.uuid.UUIDType(), nullable=False),
                        sa.Column('wurker_bee_id', sa.Integer(), nullable=False),
                        sa.Column('wurker_cron_id', sa.Integer(), nullable=True),
                        sa.Column('hive_pid', sa.Integer(), nullable=True),
                        sa.Column('hive_timestamp', sa.DateTime(), nullable=True),
                        sa.Column('hive_runcount', sa.Integer(), nullable=True),
                        sa.Column('hive_duration', sa.Integer(), nullable=True),
                        sa.Column('status', sa.Enum('PROCESSING', 'OVERRUN', 'TIMEOUT', 'ERROR', 'COMPLETED',
                                                    name='wurkerbeestate'), nullable=False),
                        sa.Column('status_info', sa.JSON(), nullable=True),
                        sa.Column('created_at', sa.DateTime(), server_default=sa.text('now()'), nullable=True),
                        sa.Column('updated_at', sa.DateTime(),
                                  server_default=sa.text('CURRENT_TIMESTAMP ON UPDATE CURRENT_TIMESTAMP'),
                                  nullable=True),
                        sa.ForeignKeyConstraint(['app_id'], ['wurker_control.app_id'], ondelete='CASCADE'),
                        sa.ForeignKeyConstraint(['wurker_bee_id'], ['wurker_bee.id'], ondelete='CASCADE'),
                        sa.ForeignKeyConstraint(['wurker_cron_id'], ['wurker_cron.id'], ondelete='CASCADE'),
                        sa.PrimaryKeyConstraint('id')
                        )

    if not sa.inspect(op.get_bind()).has_table('wurker_queue'):
        op.create_table('wurker_queue',
                        sa.Column('wurker_bee_id', sa.Integer(), nullable=False),
                        sa.Column('app_id', sqlalchemy_utils.types.uuid.UUIDType(), nullable=False),
                        sa.Column('args', sa.JSON(), nullable=True),
                        sa.Column('created_at', sa.DateTime(), server_default=sa.text('now()'), nullable=True),
                        sa.ForeignKeyConstraint(['app_id'], ['wurker_control.app_id'], ),
                        sa.ForeignKeyConstraint(['wurker_bee_id'], ['wurker_bee.id'], ),
                        sa.PrimaryKeyConstraint('wurker_bee_id')
                        )


def downgrade() -> None:
    if sa.inspect(op.get_bind()).has_table('wurker_queue'):
        op.drop_table('wurker_queue')

    if sa.inspect(op.get_bind()).has_table('wurker_bee_report'):
        op.drop_table('wurker_bee_report')

    if sa.inspect(op.get_bind()).has_table('wurker_bee_cron'):
        op.drop_table('wurker_bee_cron')

    if sa.inspect(op.get_bind()).has_table('wurker_bee'):
        op.drop_table('wurker_bee')

    if sa.inspect(op.get_bind()).has_table('wurker_status'):
        op.drop_table('wurker_status')

    if sa.inspect(op.get_bind()).has_table('wurker_cron'):
        op.drop_table('wurker_cron')

    if sa.inspect(op.get_bind()).has_table('wurker_command'):
        op.drop_table('wurker_command')

    if sa.inspect(op.get_bind()).has_table('wurker_control'):
        op.drop_table('wurker_control')
