#!/usr/bin/env python3

import os
import sys
import errno
import signal
import traceback

import wurker
from wurker.util import init_logger
from wurker.core import Globals, Wurkman


def _interrupt(signum, frame):
    Globals.STOP = True


def main():
    """The 'main' function."""

    app_root = os.path.dirname(os.path.abspath(__file__))
    dotenv_path = os.path.join(app_root, ".env")

    # Config myself
    try:
        args = wurker.parse_args(app_root)
    except Exception as ex:
        traceback.print_exc()
        return str(ex)

    # Sting, Seed, Service and Status subcommands get special treatment
    if args.subcommand in ["sting", "seed", "status", "service",]:
        return args.func(args)

    # Initialize the logger from args
    init_logger(args)

    # Die nice on all likely OS interrupts
    signal.signal(signal.SIGHUP, _interrupt)
    signal.signal(signal.SIGTERM, _interrupt)

    # Run the command with CLI args
    try:
        wurkman = Wurkman(args)
        error = args.func(wurkman)
    except KeyboardInterrupt:
        Globals.STOP = True
    except Exception as ex:
        error = ex
        traceback.print_exc()
    finally:
        Globals.STOP = True
        if args.subcommand == "run":
            wurkman.hive.clear()

    return f"ERROR: {error}" if error else None


################################################################################

if __name__ == "__main__":
    sys.exit(main())
